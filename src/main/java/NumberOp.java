import java.util.List;
import java.util.Optional;

public class NumberOp {

    private final List<NumberPrettifier> numberPrettifiers;

    public NumberOp(List<NumberPrettifier> numberPrettifiers) {
        this.numberPrettifiers = numberPrettifiers;
    }

    public String prettify(double number) {

        double powerOfTen = Math.log10(number);

        Optional<NumberPrettifier> optionalNumberPrettifier = numberPrettifiers.stream().filter(numberPrettifier -> numberPrettifier.apply(powerOfTen)).findFirst();

        if (optionalNumberPrettifier.isPresent()) {
            return optionalNumberPrettifier.get().prettify(number);
        }

        if (number == (long) number) {
            return String.valueOf((long) number);
        }

        return String.valueOf(number);
    }
}
