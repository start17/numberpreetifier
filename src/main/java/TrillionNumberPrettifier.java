public class TrillionNumberPrettifier extends NumberPrettifier {

    public TrillionNumberPrettifier() {
        super(12, "T");
    }
}
