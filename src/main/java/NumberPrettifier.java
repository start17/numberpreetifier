public abstract class NumberPrettifier {

    private final int powerOfTen;
    private final String symbol;

    protected NumberPrettifier(int powerOfTen, String symbol) {
        this.powerOfTen = powerOfTen;
        this.symbol = symbol;
    }

    public String prettify(double number) {

        double value = number / (Math.pow(10, powerOfTen));

        if (value == (int) value) {
            return String.format("%d%s", (int) value, symbol);
        }

        return String.format("%.1f%s", value, symbol);
    }

    public boolean apply(double powerOfTen) {
        return powerOfTen >= this.powerOfTen && powerOfTen < this.powerOfTen + 3;
    }
}
