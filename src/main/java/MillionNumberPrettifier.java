public class MillionNumberPrettifier extends NumberPrettifier {

    public MillionNumberPrettifier() {
        super(6, "M");
    }
}
