public class BillionNumberPrettifier extends NumberPrettifier {

    public BillionNumberPrettifier() {
        super(9, "B");
    }
}
