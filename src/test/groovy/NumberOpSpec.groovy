import spock.lang.Specification

class NumberOpSpec extends Specification {

    def 'should prettify numbers'() {

        given:
        NumberOp numberOp = new NumberOp([new MillionNumberPrettifier(), new BillionNumberPrettifier(), new TrillionNumberPrettifier()])

        when:
        String prettifiedNumber = numberOp.prettify((double) number)

        then:
        prettifiedNumber == expectedPrettifiedNumber

        where:
        number        | expectedPrettifiedNumber
        1000000       | '1M'
        1000000000    | '1B'
        1000000000000 | '1T'
        1123456789    | '1.1B'
        11123456789   | '11.1B'
        111123456789  | '111.1B'
        532           | '532'
        2500000.34    | '2.5M'
    }
}
